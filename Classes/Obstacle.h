#pragma once
#include <SFML/Graphics.hpp>
#include "Ball.h"

class Obstacle{
private:
public:
    sf::Sprite img;
    Obstacle(sf::Texture &text, int x, int y);
    ~Obstacle();
    void Draw(sf::RenderWindow &Window);
    bool isHit(Ball ball);
    void Destroy();
};
