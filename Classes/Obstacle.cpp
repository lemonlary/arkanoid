#include "Obstacle.h"
#include <iostream>

Obstacle::Obstacle(sf::Texture &text, int x, int y){
    img.setTexture(text);
    img.setPosition(x,y);
}
Obstacle::~Obstacle(){
}
void Obstacle::Draw(sf::RenderWindow &Window){
    Window.draw(img);
}
void Obstacle::Destroy(){

}
bool Obstacle::isHit(Ball ball){
    if(ball.sprite.getPosition().x < img.getPosition().x + img.getTexture()->getSize().x && ball.sprite.getPosition().x + ball.tex.getSize().x > img.getPosition().x &&
       ball.sprite.getPosition().y + ball.tex.getSize().y > img.getPosition().y && ball.sprite.getPosition().y < img.getPosition().y + img.getTexture()->getSize().y)
            return true;
    else
        return false;

}
