#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"


class Ball{
public:
    sf::Vector2f pos;
    bool isFreeze = false;
    sf::Texture tex;
    sf::Sprite sprite;
    Ball(sf::Vector2f, sf::Vector2f);
    Ball(Player, sf::RenderWindow &);
    ~Ball();
    void Update(Player, sf::RenderWindow &);
    void Draw(sf::RenderWindow &);
    void ChangePower();
    bool IsFreeze();
    void Freeze(bool);
};
