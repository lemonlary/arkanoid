#include "Board.h"

Map::Map(){
    g_tex.loadFromFile("./Data/obstacle1.png");
}
Map::~Map(){

}
void Map::Load(string Dir){
    file.open(Dir.c_str(), ios::in);
    if(file.good() == true){

        file >> width >> height;

        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                int type;
                file >> type;

                if(type == 1){
                    Objects.push_back(Obstacle(g_tex, i*210, 100+j*30));
                }
            }
        }
    file.close();
    }
}
void Map::Draw(sf::RenderWindow &win){
    for(int i = 0; i < Objects.size(); i++){
        Objects[i].Draw(win);
    }
}
void Map::CheckHit(Ball &ball, int &score){
    for(int i = 0; i < Objects.size(); i++){
        if(Objects[i].isHit(ball)){
            Objects.erase(Objects.begin() + i);
            score += 50;
            ball.ChangePower();
        }
    }
}

int Map::GetBricks(){
    return Objects.size();
}
