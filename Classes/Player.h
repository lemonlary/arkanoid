#pragma once
#include <SFML/Graphics.hpp>

class Player {
public:
    sf::Sprite img;
    sf::Texture tex;
    Player(sf::RenderWindow &Window);
    ~Player();
    void Move();
    void Draw(sf::RenderWindow &Window);

};
