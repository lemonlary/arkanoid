#pragma once
#include <fstream>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Obstacle.h"
#include <vector>
#include <string>

using namespace std;

class Map{
private:
    int lvl;
public:
    sf::Texture g_tex;
    vector<Obstacle> Objects;
    fstream file;
    int width;
    int height;
    Map();
    ~Map();
    void Load(string Dir);
    void Draw(sf::RenderWindow &win);
    void CheckHit(Ball &ball, int &score);
    int GetBricks();
};
