#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Ball.h"
#include "Obstacle.h"
#include "Board.h"
#include <iostream>
#include <string>
#include <sstream>

enum GameState {
    GS_PLAY,
    GS_PAUSE,
    GS_MENU,
    GS_WIN,
    GS_LOSE,
    GS_END
};

class Game {
private:
    sf::Font font;
    GameState state;
    unsigned int lifes;
    int score = 0;
public:
    int actuallvl;
    Game();
    ~Game();
    bool Check();
    void Menu();
    void RunGame();
    //void Pause();
    void EndGame();
    void NextMap(int lvl, Map &mapa);
};

