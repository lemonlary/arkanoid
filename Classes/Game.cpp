#include "Game.h"

sf::RenderWindow okno(sf::VideoMode(1080, 840), "Arkanoid - lemonlary");
const float FPS = 120.0f;
std::ostringstream tostring;

Game::Game(){
    state = GS_END;

	if(!font.loadFromFile("Data/deffont.ttf"))
	{
		std::cout << "Brak czcionki!" << std::endl;
		return;
	}

	state = GS_MENU;

}
Game::~Game(){
}
bool Game::Check(){

}
void Game::Menu(){
    sf::Texture bg;
    bg.loadFromFile("Data/backingame.png");
    sf::Sprite background;
    background.setTexture(bg);

    sf::Text title("Arkanoid by lemonlary", font, 42);
    title.setPosition(okno.getSize().x/2-title.getGlobalBounds().width/2, 100);

    const int ile = 2;

    std::string Options[] = {"Graj", "Wyjdz"};
    sf::Text text[ile];
    for(int i = 0; i < ile; i++){
        text[i].setFont(font);
		text[i].setCharacterSize(32);
        text[i].setString(Options[i]);
		text[i].setPosition(okno.getSize().x/2-text[i].getGlobalBounds().width/2,250+i*120);
    }

    while(state == GS_MENU){
        sf::Vector2f mouse(sf::Mouse::getPosition(okno));
		sf::Event event;
		while(okno.pollEvent(event)){
            if(event.type == sf::Event::Closed || event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
                state = GS_END;
            else if(text[1].getGlobalBounds().contains(mouse) && event.type == sf::Event::MouseButtonReleased && event.key.code == sf::Mouse::Left){
                state = GS_END;
			}
			else if(text[0].getGlobalBounds().contains(mouse) && event.type == sf::Event::MouseButtonReleased && event.key.code == sf::Mouse::Left){
                RunGame();
			}
		}
		for(int i=0;i<ile;i++)
			if(text[i].getGlobalBounds().contains(mouse))
				text[i].setColor(sf::Color::Red);
			else
                text[i].setColor(sf::Color::White);

        okno.clear(sf::Color::Black);
        okno.draw(background);
        for(int i = 0; i < ile; i++)
            okno.draw(text[i]);
        okno.draw(title);
        okno.display();
    }

}
void Game::RunGame(){
    state = GS_PLAY;
    sf::Clock clock;
    sf::Time time;

    sf::Texture bg;
    bg.loadFromFile("Data/backingame.png");
    sf::Sprite background;
    background.setTexture(bg);

    lifes = 3;
    tostring << lifes;
    std::string strlifes = tostring.str();
    sf::Text life("Zycia: "+strlifes, font, 23);
    life.setPosition(15, 15);
    life.setColor(sf::Color::Blue);

    Map map;
    map.Load("Maps/map_1.txt");
    actuallvl = 1;

    Player player(okno);
    Ball ball(player, okno);
    ball.isFreeze = true;
    ball.pos = sf::Vector2f(5, -5);

    while(state == GS_PLAY){
        if(time.asSeconds() > (1.0f / FPS)){
            clock.restart();
            time = sf::Time::Zero;

            sf::Event event;
            while(okno.pollEvent( event ) ){
                if(event.type == sf::Event::Closed)
                     state = GS_END;
                else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
                     ball.Freeze(false);
                }
            }
            if(ball.sprite.getPosition().y + ball.tex.getSize().y == okno.getSize().y){
                ball.Freeze(true);
                lifes--;

                tostring.str("");
                tostring << lifes;
                strlifes = tostring.str();
                life.setString("Zycia: "+strlifes);
                if(lifes == 0){
                    tostring.str("");
                    EndGame();
                }
            }

            player.Move();
            ball.Update(player, okno);
            map.CheckHit(ball, score);

            if(map.GetBricks() <= 0){
                actuallvl++;
                NextMap(actuallvl, map);
            }

        }

        okno.clear(sf::Color::Black);
        okno.draw(background);
        map.Draw(okno);
        ball.Draw(okno);
        player.Draw(okno);
        okno.draw(life);
        okno.display();

        time = clock.getElapsedTime();
    }
}

void Game::NextMap(int lvl, Map &mapa){
    switch(lvl){
        case 1:
            mapa.Load("Maps/map_1.txt");
            break;
        case 2:
            mapa.Load("Maps/map_2.txt");
            break;
        case 3:
            mapa.Load("Maps/map_3.txt");
            break;
        case 4:
            mapa.Load("Maps/map_4.txt");
            break;
        case 5:
            mapa.Load("Maps/map_5.txt");
            break;
        case 6:
            mapa.Load("Maps/map_6.txt");
            break;
        default:
            mapa.Load("Maps/map_1.txt");
            break;
    }
}

void Game::EndGame(){
    state = GS_LOSE;

    sf::Texture bg;
    bg.loadFromFile("Data/backingame.png");
    sf::Sprite background;
    background.setTexture(bg);

    sf::Text title("Przegrales!", font, 42);
    title.setPosition(okno.getSize().x/2-title.getGlobalBounds().width/2, 100);

    tostring.str("");
    tostring << score;
    std::string myscore = tostring.str();
    sf::Text score("Twoj wynik to: "+myscore, font, 25);
    score.setPosition(okno.getSize().x/2-score.getGlobalBounds().width/2, 150);
    tostring.str("");

    const int ile = 2;

    std::string Options[] = {"Jeszcze raz!", "Wyjdz"};
    sf::Text text[ile];
    for(int i = 0; i < ile; i++){
        text[i].setFont(font);
		text[i].setCharacterSize(32);
        text[i].setString(Options[i]);
		text[i].setPosition(okno.getSize().x/2-text[i].getGlobalBounds().width/2,250+i*80);
    }

    while(state == GS_LOSE){
        sf::Vector2f mouse(sf::Mouse::getPosition(okno));
		sf::Event event;
		while(okno.pollEvent(event)){
            if(event.type == sf::Event::Closed || event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
                state = GS_END;
            else if(text[1].getGlobalBounds().contains(mouse) && event.type == sf::Event::MouseButtonReleased && event.key.code == sf::Mouse::Left){
                state = GS_END;
			}
			else if(text[0].getGlobalBounds().contains(mouse) && event.type == sf::Event::MouseButtonReleased && event.key.code == sf::Mouse::Left){
                RunGame();
			}
		}
		for(int i=0;i<ile;i++)
			if(text[i].getGlobalBounds().contains(mouse))
				text[i].setColor(sf::Color::Red);
			else
                text[i].setColor(sf::Color::White);

        okno.clear(sf::Color::Black);
        okno.draw(background);
        for(int i = 0; i < ile; i++)
            okno.draw(text[i]);
        okno.draw(title);
        okno.draw(score);
        okno.display();
    }
}


