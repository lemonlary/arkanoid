#include "Ball.h"

Ball::Ball(sf::Vector2f poss, sf::Vector2f pow){
    pos = pow;
    tex.loadFromFile("Data/ball.png");
    sprite.setTexture(tex);
    sprite.setPosition(poss);
}
Ball::Ball(Player pid, sf::RenderWindow &win){
    tex.loadFromFile("Data/ball.png");
    sprite.setTexture(tex);
    sprite.setPosition(pid.img.getPosition().x + pid.tex.getSize().x / 2, pid.img.getPosition().y - tex.getSize().y);
}
Ball::~Ball(){

}
void Ball::Update(Player player, sf::RenderWindow &win){

    if(!isFreeze){
        if(sprite.getPosition().y + tex.getSize().y >= player.img.getPosition().y && player.img.getPosition().x + player.tex.getSize().x >= sprite.getPosition().x&&
        player.img.getPosition().x <= sprite.getPosition().x + tex.getSize().x){
            pos.y = -5;
        }
    } else {
        sprite.setPosition(player.img.getPosition().x + player.tex.getSize().x / 2, player.img.getPosition().y - tex.getSize().y);
    }

    if(sprite.getPosition().x <= 0){
        pos.x = 5;
    }
    else if(sprite.getPosition().x + tex.getSize().x >= win.getSize().x){
        pos.x = -5;
    }
    else if(sprite.getPosition().y <= 0){
        pos.y = 5;
    }
    else if(sprite.getPosition().y + tex.getSize().y >= win.getSize().y){
        pos.y = -5;
    }

    if(!isFreeze)
        sprite.move(pos.x, pos.y);
}

void Ball::Draw(sf::RenderWindow &win){
    win.draw(sprite);
}

void Ball::ChangePower(){
    if(pos.y == -5)
        pos.y = 5;
    else if(pos.y == 5)
        pos.y = -5;

    if(pos.x == 5)
        pos.x = 5;
    else if(pos.x == -5)
        pos.x = -5;
}

bool Ball::IsFreeze(){
    return isFreeze;
}

void Ball::Freeze(bool flag){
    isFreeze = flag;
}
