#include "Player.h"

Player::Player(sf::RenderWindow &Window){
    tex.loadFromFile("Data/platform.png");
    img.setTexture(tex);
    img.setPosition(Window.getSize().x/2,Window.getSize().y - 90);
}
Player::~Player(){
}
void Player::Move(){
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        if(img.getPosition().x > 0)
            img.move(-10,0);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        if(img.getPosition().x + tex.getSize().x < 1080)
            img.move(10,0);
    }
}
void Player::Draw(sf::RenderWindow &Window){
    Window.draw(img);
}
